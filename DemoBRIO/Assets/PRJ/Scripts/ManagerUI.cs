﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagerUI 
{

    #region Methods

    #region Public

    public string UpdateText(int nowObj, int maxObj)
    {
       return "Receivers " + nowObj.ToString() + "/" + maxObj.ToString();
    }

    public string UpdateText(int nowObj)
    {
        return "Transmitters = " + nowObj.ToString();
    }
    #endregion

    #endregion

}
