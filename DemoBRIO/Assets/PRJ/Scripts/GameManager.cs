﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Public

    [Header("Настройки сигналов")]
    
    [Tooltip("Скрость переимещения сигнала")]
    [Range(2, 10)]
    public float Speed = 10;

    [Tooltip("Время появления сигнала")]
    [Range(0.5f, 5)]
    public float TimeSpawn = 1f;

    public static GameManager instance { get; private set; }

    [Header("Натсройки Приемников")]
    public List<GameObject> Recievers;
    
    #endregion

    #region Private

    private PlaceObjectOnBackground placeObj;

    #endregion

    #region Methods

    #region Private

    private void Awake()
    {
         instance = this;
    }

    private void Start()
    {
        placeObj = GetComponent<PlaceObjectOnBackground>();
        Recievers = placeObj.Receivers;
    }

    #endregion

    #endregion
}
