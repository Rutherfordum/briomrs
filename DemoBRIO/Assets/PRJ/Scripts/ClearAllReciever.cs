﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
// удаление всех ресиверов при запуске
public class ClearAllReciever : MonoBehaviour
{
    #region Private

    private List<GameObject> inSceneReceiversAll = new List<GameObject>();

    #endregion

    #region Methods

    #region Private

    private void Awake()
    {
        DestroyReceivers();
    }

    // перед стартом удляем все забытые прередачики 
    private void DestroyReceivers()
    {
        inSceneReceiversAll.Clear();        
        inSceneReceiversAll.AddRange(GameObject.FindGameObjectsWithTag("Receiver"));
        if (inSceneReceiversAll.Count != 0)
        {
            foreach (GameObject item in inSceneReceiversAll)
            {
                Destroy(item);                
            }
            inSceneReceiversAll = new List<GameObject>();
        }
    }

    #endregion

    #endregion
}
