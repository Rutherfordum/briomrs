﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Signal : MonoBehaviour
{

    // если захотите использовать другие Тэги
    enum TagsGO{
        Receiver,
        other
    }

    #region Public
    //цель куда надо стрельнуть
    public GameObject target;
    #endregion

    #region Private

    private ObjectPooler _objPooler;
    private float _speed = 10f;    // скорость
    private TagsGO _tagCollider = TagsGO.Receiver;// Тэг обьекта на сцене

    #endregion

    #region Methods

    #region Private

    private void Start()
    {
        // получим ссылку на обьект
        _objPooler = GetComponentInParent<ObjectPooler>();
        target = transform.GetComponentInParent<ObjectPooler>().TargetReceiver;
        _speed = _objPooler._speed;
    }

    // добавляем в басейн после удара с обьектом
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == _tagCollider.ToString())
        {
            _objPooler.AddtoPool(gameObject);
        }
    }

    // при включении сигнала дать позиции передатчика
    private void OnEnable()
    {
        transform.position = transform.GetComponentInParent<ObjectPooler>().StartPosition.transform.position;
    }

    private void Update()
    {
        MoveToReceiver(target);
    }
    // перемещение обьекта
    private void MoveToReceiver(GameObject receiver)
    {
        //transform.LookAt(receiver.transform.position);
        transform.position = Vector3.MoveTowards(transform.position, receiver.transform.position, Time.deltaTime * _speed);
    }

    #endregion

    #endregion
}
