﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPooler : MonoBehaviour
{
    #region Public
    public GameObject TargetReceiver;// Цель
    public GameObject StartPosition; // наш передатчик
    public GameManager gameManager;
    //public static ObjectPooler instance { get; private set; }
    #endregion

    #region Private

    // использую Queue  для патерна ObjectPool
    private Queue<GameObject> PooledObjects = new Queue<GameObject>();
    protected float _timeSpawn;
    protected float _lastTime;
    public float _speed;

    #endregion

    #region Methods

    #region Private

    private void Awake()
    {
        gameManager = GameObject.FindObjectOfType<GameManager>();
        _timeSpawn = gameManager.TimeSpawn;
        _speed = gameManager.Speed;
        // instance = this;
        GrowPool();
    }

    private void Update()
    {
        if (Time.time - _lastTime > _timeSpawn)
        {
            SpawnFromPool();
        }
    }

    // выгружаем с таймером наш бассейн
    private void SpawnFromPool()
    {
        _lastTime = Time.time;
        GameObject signal = GetFromPool();
    }

    // заполняем бассейн
    private void GrowPool()
    {
        for (int i = 0; i < 10; i++)
        {
            GameObject signalPref = Instantiate(Resources.Load<GameObject>("Prefabs/Signal"), transform.Find("Signals " + i.ToString()), true);
            signalPref.transform.SetParent(transform);
            AddtoPool(signalPref);
        }
    }

    #endregion

    #region Public

    // выгружаем из бассейна наш сигнал
    public GameObject GetFromPool()
    {
        if (PooledObjects.Count == 0)
        {
            GrowPool();
        }

        var signal = PooledObjects.Dequeue();
        signal.SetActive(true);
        return signal;
    }

    // добовляем наши сигнали в бассейн
    public void AddtoPool(GameObject signal)
    {
        signal.transform.position = transform.position;
        signal.SetActive(false);
        PooledObjects.Enqueue(signal);
    }

    #endregion

    #endregion
}
