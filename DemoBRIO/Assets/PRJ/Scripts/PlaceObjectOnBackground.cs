﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
// размещение обьектов на сцене по клику
public class PlaceObjectOnBackground : MonoBehaviour
{
    #region SerializeField

    [SerializeField] private LayerMask layearObj; 
    [SerializeField] private Text receiverCount;
    [SerializeField] private Text transmitterCount;

    #endregion

    #region Private

    private GameObject _receiver;
    private ManagerUI _managerUI = new ManagerUI();

    #endregion

    #region Public

    [Range(1, 5)]
    public int MaxPlaceObject;
    [HideInInspector] public List<GameObject> Receivers = new List<GameObject>();
    [HideInInspector] public List<GameObject> Transmitters = new List<GameObject>();
    
    #endregion

    #region Methods

    #region Private

    private void Start()
    {
        this.gameObject.GetComponent<GameManager>().enabled = false;

        OutTextUI(receiverCount, Receivers, "Receiver", true);
        OutTextUI(transmitterCount, Transmitters, "Transmitter", false);
      
        // проверка на обьект, если нет добовляем из ресурсов
        if (_receiver == null)
        {
            _receiver = Resources.Load<GameObject>("Prefabs/Receiver(приемник)");
        }

        if (receiverCount == null)
        {
            Debug.LogWarning("нет ссылки на обьект " + receiverCount.name);
        }
    }

    private void Update()
    {
        PlaceObject(_receiver);
    }

    // размещение обьектов на сцене
    private void PlaceObject(GameObject _object)
    {
        //размещение обьекта по позиции мыши и его клику и слою
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Input.GetMouseButtonUp(0))
        {
            if (Physics.Raycast(ray, out hit, 100, layearObj))
            {
                if (_receiver != null)
                {
                    // размещение обьекта и проверка максимального количества обьектов, если максимум отключаем этот скрипт
                    Receivers.Add(Instantiate(_object, hit.point, Quaternion.identity));
                    if (Receivers.Count >= MaxPlaceObject)
                    {
                        this.gameObject.GetComponent<PlaceObjectOnBackground>().enabled = false;
                        this.gameObject.GetComponent<GameManager>().enabled = true;

                        foreach (GameObject item in Transmitters)
                        {
                            item.GetComponent<Transmitter>().enabled = true;
                        }
                       // Tra
                    }
                    //обновляем счетсик приемников   
                    OutTextUI(receiverCount, Receivers, "Receiver", true);
                    /* if (receiverCount != null)
                     {
                         receiverCount.text = _managerUI.UpdateText(Receivers.Count, MaxPlaceObject);
                     }*/
                }
                else
                {
                    Debug.LogError("not found gameobject");
                }
            }
        }
    }

    // поиск обьектов на сцене и вывод в UI
    private void OutTextUI(Text textUI, List<GameObject> objects, string tagObjects, bool value)
    {
        objects.Clear();
        objects.AddRange(GameObject.FindGameObjectsWithTag(tagObjects));

        //для Receiver
        if (textUI != null && value)
        {
            textUI.text = _managerUI.UpdateText(objects.Count, MaxPlaceObject);
        }
        //для Transmitter
        if (textUI != null && !value)
        {
            textUI.text = _managerUI.UpdateText(objects.Count);
        }
    }

    #endregion

    #endregion
}
