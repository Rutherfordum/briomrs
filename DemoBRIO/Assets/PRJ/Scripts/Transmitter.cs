﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transmitter : MonoBehaviour
{
    #region SerializeField

    [Range(2f, 6f)]
    [SerializeField] private float limitMove = 5f;

    [Range(2f, 10f)]
    [SerializeField] private float speed = 5f;

    [SerializeField] private int signalsCount = 30;
    #endregion

    #region Private

    private Vector3 targetposition;

    #endregion

    #region Methods

    #region Private

    private void Start()
    {
        // создаем пустые обьекты для красивой иерархии бассейнас с обьектами сигнал (каждый отвечает за 1 приемник) 
        // для каждого в сцене передатчика
        int maxReceivers = GameObject.FindObjectOfType<PlaceObjectOnBackground>().MaxPlaceObject;

        for (int i = 0; i < maxReceivers; i++)
        {
            GameObject go = new GameObject("Signals " + i.ToString()); ;
            go.AddComponent<ObjectPooler>();
            go.GetComponent<ObjectPooler>().TargetReceiver = GameObject.FindObjectOfType<PlaceObjectOnBackground>().Receivers[i];
            go.GetComponent<ObjectPooler>().StartPosition = this.gameObject;
        }

        targetposition = new Vector3(Random.Range(-limitMove, limitMove), Random.Range(-limitMove, limitMove), 0f);
    }

    private void Update()
    {
        RandomMove();
    }

    private void OnMouseDrag()
    {
        FolowMouse();
    }

    private void OnMouseUp()
    {
        transform.localScale = new Vector3(1, 1, 1);
    }

    // следует за курсором мыши
    private void FolowMouse()
    {
        transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
        Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10f);
        transform.position = Camera.main.ScreenToWorldPoint(mousePosition);
    }

    // рандомное перемещение обьекта 
    private void RandomMove()
    {
        if (transform.position == targetposition)
        {
            targetposition = new Vector3(Random.Range(-limitMove, limitMove), Random.Range(-limitMove, limitMove), 0f);
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, targetposition, Time.deltaTime * speed);
        }
    }

    private void test()
    {

    }
    #endregion

    #endregion
}
